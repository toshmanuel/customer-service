package com.crowninitiative.customerservice.service;

import com.crowninitiative.customerservice.dto.request.RegisterCustomerRequest;
import com.crowninitiative.customerservice.exception.CustomerException;
import com.crowninitiative.customerservice.model.Billing;
import com.crowninitiative.customerservice.model.Customer;
import com.crowninitiative.customerservice.model.Tariff;
import com.crowninitiative.customerservice.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
class CustomerServiceImplTest {

    @Mock
    private CustomerRepository mockCustomerRepository;

    @Mock
    private BillingService mockBillingService;
    private CustomerService customerService;
    @BeforeEach
    void setUp() {
        customerService = new CustomerServiceImpl(mockCustomerRepository, mockBillingService);
    }

    @Test
    void testToSaveCustomer() {
        Billing newBilling = new Billing("billing-001","1234567890-01", Tariff.POSTPAID);
        Customer newCustomer = new Customer("cust-001", "new", "customer", "new@customer.com", newBilling);

        when(mockBillingService.saveBilling(any(Billing.class))).thenReturn(newBilling);
        when(mockCustomerRepository.save(any(Customer.class))).thenReturn(newCustomer);

        RegisterCustomerRequest request = new RegisterCustomerRequest("new", "customer", "new@customer.com", "1234567890", Tariff.POSTPAID);
        String response = customerService.createCustomer(request);

        verify(mockBillingService, times(1)).saveBilling(any(Billing.class));
        verify(mockCustomerRepository, times(1)).save(any(Customer.class));
        assertThat(response).isEqualTo("Customer cust-001 created successfully");
    }

    @Test
    void testToFindACustomerById() {
        Billing newBilling = new Billing("billing-001","1234567890-01", Tariff.POSTPAID);
        Customer newCustomer = new Customer("cust-001", "new", "customer", "new@customer.com", newBilling);

        when(mockCustomerRepository.findById(anyString())).thenReturn(Optional.of(newCustomer));
        customerService.getCustomer("cust-001");
        verify(mockCustomerRepository, times(1)).findById(anyString());
    }

    @Test
    void testThatCustomerIdThatDoesNotExistWillThrowException(){
        when(mockCustomerRepository.findById(anyString())).thenThrow(new CustomerException("Customer with id not found"));
        CustomerException exception = assertThrows(CustomerException.class, () -> customerService.getCustomer("001"));
        verify(mockCustomerRepository, times(1)).findById("001");
        assertThat(exception.getMessage()).isEqualTo("Customer with id not found");
    }

    @Test
    void testToGetAllCustomer(){
        Billing newBilling = new Billing("billing-001","1234567890-01", Tariff.POSTPAID);
        Customer newCustomer = new Customer("cust-001", "new", "customer", "new@customer.com", newBilling);

        when(mockCustomerRepository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(Collections.singletonList(newCustomer)));
        List<Customer> customers = customerService.getAllCustomers(PageRequest.of(0, 9));
        verify(mockCustomerRepository, times(1)).findAll(any(Pageable.class));
        assertThat(customers.size()).isEqualTo(1);
    }
}