package com.crowninitiative.customerservice.service;

import com.crowninitiative.customerservice.model.Billing;
import com.crowninitiative.customerservice.model.Tariff;
import com.crowninitiative.customerservice.repository.BillingRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
class BillingServiceTest {

    @Mock
    private BillingRepository billingRepository;

    private  BillingService billingService;

    @BeforeEach
    void setUp() {
        billingService = new BillingServiceImpl(billingRepository);
    }

    @Test
    void testToSaveBilling() {
        Billing billing = new Billing("billing-001", "12344656654-01", Tariff.POSTPAID);

        Billing testBilling = new Billing("12344656654", Tariff.POSTPAID);
        when(billingRepository.save(testBilling)).thenReturn(billing);
        Billing billingResult = billingService.saveBilling(testBilling);
        System.out.println(billingResult);
        verify(billingRepository, times(1)).save(testBilling);
    }
}