package com.crowninitiative.customerservice.repository;

import com.crowninitiative.customerservice.model.Customer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataMongoTest
class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;
    private Customer customer1;
    private Customer customer2;
    private Customer customer3;
    @BeforeEach
    void setUp() {
        customer1 = new Customer("cust_1", "first", "one", "first.one@email.com", null);
        customer2 = new Customer("cust_2", "second", "two", "second.two@email.com", null);
        customer3 = new Customer("cust_3", "third", "three", "third.three@email.com", null);
        customerRepository.saveAll(Arrays.asList(customer1, customer2, customer3));
    }

    @AfterEach
    void tearDown() {
        customerRepository.deleteAll();
    }

    @Test
    void testToSaveCustomer(){
        Customer newCustomer = new Customer("cust_new", "new", "customer", "new@customer.com", null);
        customerRepository.save(newCustomer);
        assertThat(customerRepository.count()).isEqualTo(4);
    }

    @Test
    void testToGetCustomerById() {
        Optional<Customer> optionalCustomer = customerRepository.findById("cust_1");
        assertTrue(optionalCustomer.isPresent());
        assertThat(optionalCustomer.get().getFirstName()).isEqualTo("first");
    }

    @Test
    void testToGetAllCustomer(){
        Page<Customer> customers = customerRepository.findAll(PageRequest.of(0, 10));
        assertThat(customers.getContent().size()).isEqualTo(3);
    }
}