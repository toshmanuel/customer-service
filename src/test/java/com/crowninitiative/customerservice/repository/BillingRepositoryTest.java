package com.crowninitiative.customerservice.repository;

import com.crowninitiative.customerservice.model.Billing;
import com.crowninitiative.customerservice.model.Tariff;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
@ActiveProfiles("test")
class BillingRepositoryTest {

    @Autowired
    private BillingRepository billingRepository;
    private Billing billing1;
    private Billing billing2;
    @BeforeEach
    void setUp() {
        billing1 = new Billing("id001", "123456786", Tariff.POSTPAID);
        billing2 = new Billing("id002", "742723934", Tariff.PREPAID);
        billing2 = new Billing("id002", "742723934", Tariff.PREPAID);
        billingRepository.saveAll(Arrays.asList(billing1, billing2));
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void testToCreateBilling() {
        Billing newBilling = new Billing("id003", "7872374827", Tariff.PREPAID);
        billingRepository.save(newBilling);
        assertThat(billingRepository.count()).isEqualTo(3);
    }

    @Test
    void testToFindBillingByAccountNumber(){
        Optional<Billing> optionalBilling = billingRepository.findBillingByAccountNumber("742723934");
        assertTrue(optionalBilling.isPresent());
        assertThat(optionalBilling.get().getId()).isEqualTo("id002");
    }
}