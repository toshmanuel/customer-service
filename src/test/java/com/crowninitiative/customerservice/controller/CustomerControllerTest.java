package com.crowninitiative.customerservice.controller;

import com.crowninitiative.customerservice.dto.request.RegisterCustomerRequest;
import com.crowninitiative.customerservice.exception.BillingException;
import com.crowninitiative.customerservice.exception.CustomerException;
import com.crowninitiative.customerservice.model.Billing;
import com.crowninitiative.customerservice.model.Customer;
import com.crowninitiative.customerservice.model.Tariff;
import com.crowninitiative.customerservice.service.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@WebMvcTest({CustomerController.class})
class CustomerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService mockedCustomerService;

    @BeforeEach
    void setUp() {

    }

    @Test
    void testToCreateCustomerSuccessfully() throws Exception {
        RegisterCustomerRequest request = new RegisterCustomerRequest(
                "test",
                "testing",
                "test@test.com",
                "1234567890",
                Tariff.POSTPAID
        );
        when(mockedCustomerService.createCustomer(any(RegisterCustomerRequest.class))).thenReturn("Customer created successfully");
        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .post("/api/v1/customers")
                .content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isCreated())
                .andExpect(content().string("{\"message\":\"Customer created successfully\",\"status\":201,\"success\":true}"));
    }

    @Test
    void testToCreateCustomerWithAnInvalidEmailFormat() throws Exception {
        RegisterCustomerRequest request = new RegisterCustomerRequest(
                "test",
                "testing",
                "test.com",
                "1234567890",
                Tariff.POSTPAID
        );
        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .post("/api/v1/customers")
                .content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Invalid email format, input right email\",\"status\":400,\"success\":false}"));
    }
@Test
    void testToCreateCustomerWithAnAccountNumberLessThanTenCharacters() throws Exception {
        RegisterCustomerRequest request = new RegisterCustomerRequest(
                "test",
                "testing",
                "test@test.com",
                "12345678",
                Tariff.POSTPAID
        );
        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .post("/api/v1/customers")
                .content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"account number can only be 10 characters\",\"status\":400,\"success\":false}"));
    }
@Test
    void testToCreateCustomerWithAnAccountNumberGreaterThanTenCharacters() throws Exception {
        RegisterCustomerRequest request = new RegisterCustomerRequest(
                "test",
                "testing",
                "test@test.com",
                "12345678909890",
                Tariff.POSTPAID
        );
        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .post("/api/v1/customers")
                .content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"account number can only be 10 characters\",\"status\":400,\"success\":false}"));
    }

@Test
    void testToCreateCustomerWithAnAccountNumberContainingTextValues() throws Exception {
        RegisterCustomerRequest request = new RegisterCustomerRequest(
                "test",
                "testing",
                "test@test.com",
                "123456abcd",
                Tariff.POSTPAID
        );
        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .post("/api/v1/customers")
                .content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"account number should only contain digits\",\"status\":400,\"success\":false}"));
    }

    @Test
    void testToCreateCustomerWithAnExistingEmail() throws Exception {
        RegisterCustomerRequest request = new RegisterCustomerRequest(
                "test",
                "testing",
                "test@test.com",
                "1234567890",
                Tariff.POSTPAID
        );

        when(mockedCustomerService.createCustomer(any(RegisterCustomerRequest.class))).thenThrow(new CustomerException("This email already exist, pls try another"));

        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .post("/api/v1/customers")
                .content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"This email already exist, pls try another\",\"status\":400,\"success\":false}"));
    }
    @Test
    void testToCreateCustomerWithAnExistingAccountNumber() throws Exception {
        RegisterCustomerRequest request = new RegisterCustomerRequest(
                "test",
                "testing",
                "test@test.com",
                "1234567890",
                Tariff.POSTPAID
        );

        when(mockedCustomerService.createCustomer(any(RegisterCustomerRequest.class))).thenThrow(new BillingException("This account number already exist, pls try another"));

        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .post("/api/v1/customers")
                .content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"This account number already exist, pls try another\",\"status\":400,\"success\":false}"));
    }

    @Test
    void testThatCustomerWithAParticularIdCanBeFound() throws Exception {
        Billing newBilling = new Billing("billing-001","1234567890-01", Tariff.POSTPAID);
        Customer newCustomer = new Customer("cust-001", "new", "customer", "new@customer.com", newBilling);

        when(mockedCustomerService.getCustomer(anyString())).thenReturn(newCustomer);
        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .get("/api/v1/customers/cust-001")
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isOk())
                .andExpect(content().string("{\"id\":\"cust-001\",\"firstName\":\"new\",\"lastName\":\"customer\",\"email\":\"new@customer.com\",\"billingDetails\":{\"id\":\"billing-001\",\"accountNumber\":\"1234567890-01\",\"tariff\":\"POSTPAID\"}}"));

    }

    @Test
    void testThatCustomerWithUnknownIdWillThrowException() throws Exception {
        when(mockedCustomerService.getCustomer(anyString())).thenThrow(new CustomerException("Customer with id not found"));
        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .get("/api/v1/customers/cust-00111")
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Customer with id not found\",\"status\":400,\"success\":false}"));

    }

    @Test
    void testToGetAllCustomers() throws Exception {
        Billing newBilling = new Billing("billing-001","1234567890-01", Tariff.POSTPAID);
        Customer newCustomer = new Customer("cust-001", "new", "customer", "new@customer.com", newBilling);

        when(mockedCustomerService.getAllCustomers(any(Pageable.class))).thenReturn(Collections.singletonList(newCustomer));
        MockHttpServletRequestBuilder requestBuilders = MockMvcRequestBuilders
                .get("/api/v1/customers")
                .contentType(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilders)
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":\"cust-001\",\"firstName\":\"new\",\"lastName\":\"customer\",\"email\":\"new@customer.com\",\"billingDetails\":{\"id\":\"billing-001\",\"accountNumber\":\"1234567890-01\",\"tariff\":\"POSTPAID\"}}]"));

    }


}