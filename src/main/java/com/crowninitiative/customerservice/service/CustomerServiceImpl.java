package com.crowninitiative.customerservice.service;

import com.crowninitiative.customerservice.dto.request.RegisterCustomerRequest;
import com.crowninitiative.customerservice.exception.BillingException;
import com.crowninitiative.customerservice.exception.CustomerException;
import com.crowninitiative.customerservice.model.Billing;
import com.crowninitiative.customerservice.model.Customer;
import com.crowninitiative.customerservice.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final BillingService billingService;

    @Override
    public String createCustomer(RegisterCustomerRequest request) {
        if (customerRepository.findByEmail(request.getEmail()).isPresent()){
            throw new CustomerException("This email already exists, pls try another email");
        }
        if(billingService.findBillingDetailsByAccountNumber(request.getAccountNumber()).isPresent()){
            throw new BillingException("This account number already exists, pls try another one");
        }
        Billing billing = new Billing();
        billing.setAccountNumber(request.getAccountNumber());
        billing.setTariff(request.getTariff());
        billingService.saveBilling(billing);

        Customer customer = new Customer();
        customer.setFirstName(request.getFirstName());
        customer.setLastName(request.getLastName());
        customer.setEmail(request.getEmail());
        customer.setBillingDetails(billing);
        Customer savedCustomer = customerRepository.save(customer);
        return "Customer " + savedCustomer.getId() + " created successfully";
    }

    @Override
    public Customer getCustomer(String id) {

        return customerRepository.findById(id).orElseThrow(() ->
                new CustomerException("Customer with id: " + id + " not found"));
    }

    @Override
    public List<Customer> getAllCustomers(Pageable pageable) {
        return customerRepository.findAll(pageable).getContent();
    }

}
