package com.crowninitiative.customerservice.service;

import com.crowninitiative.customerservice.model.Billing;

import java.util.Optional;

public interface BillingService {

    Billing saveBilling(Billing billing);
    Optional<Billing> findBillingDetailsByAccountNumber(String accountNumber);
}
