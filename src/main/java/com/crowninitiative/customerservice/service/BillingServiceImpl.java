package com.crowninitiative.customerservice.service;

import com.crowninitiative.customerservice.model.Billing;
import com.crowninitiative.customerservice.repository.BillingRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class BillingServiceImpl implements BillingService{

    private final BillingRepository billingRepository;

    @Override
    public Billing saveBilling(Billing billing) {
        final String signature = "-01";
        billing.setAccountNumber(billing.getAccountNumber() + signature);
        return billingRepository.save(billing);
    }

    @Override
    public Optional<Billing> findBillingDetailsByAccountNumber(String accountNumber) {
        return billingRepository.findBillingByAccountNumber(accountNumber + "-01");
    }
}
