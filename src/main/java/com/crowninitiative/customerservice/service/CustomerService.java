package com.crowninitiative.customerservice.service;

import com.crowninitiative.customerservice.dto.request.RegisterCustomerRequest;
import com.crowninitiative.customerservice.model.Customer;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CustomerService {
    String createCustomer(RegisterCustomerRequest request);
    Customer getCustomer(String id);

    List<Customer> getAllCustomers(Pageable pageable);
}
