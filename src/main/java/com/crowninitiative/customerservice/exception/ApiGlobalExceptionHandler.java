package com.crowninitiative.customerservice.exception;

import com.crowninitiative.customerservice.dto.response.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.Objects;

@RestControllerAdvice
public class ApiGlobalExceptionHandler {

    @ExceptionHandler(CustomerException.class)
    public ResponseEntity<?> handleException(CustomerException e) {
        ApiResponse response = new ApiResponse(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST.value(), false);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BillingException.class)
    public ResponseEntity<?> handleException(BillingException e) {
        ApiResponse response = new ApiResponse(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST.value(), false);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> handleException(MethodArgumentNotValidException e) {
        String message = Objects.requireNonNull(e.getFieldError()).getDefaultMessage();
        ApiResponse response = new ApiResponse(message, HttpStatus.BAD_REQUEST.value(), false);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleException(ConstraintViolationException e){
        String message = e.getLocalizedMessage();
        ApiResponse response = new ApiResponse(message, HttpStatus.BAD_REQUEST.value(), false);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
