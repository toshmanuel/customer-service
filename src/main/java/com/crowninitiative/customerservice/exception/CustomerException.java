package com.crowninitiative.customerservice.exception;

public class CustomerException extends ApplicationException {

    public CustomerException(String message) {
        super(message);
    }
}
