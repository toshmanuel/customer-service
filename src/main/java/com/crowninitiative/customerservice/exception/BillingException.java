package com.crowninitiative.customerservice.exception;

public class BillingException extends ApplicationException {
    public BillingException(String message) {
        super(message);
    }
}
