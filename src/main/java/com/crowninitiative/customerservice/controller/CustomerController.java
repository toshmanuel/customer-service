package com.crowninitiative.customerservice.controller;

import com.crowninitiative.customerservice.dto.request.RegisterCustomerRequest;
import com.crowninitiative.customerservice.dto.response.ApiResponse;
import com.crowninitiative.customerservice.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/customers")
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("")
    public ResponseEntity<ApiResponse> create(@RequestBody @Valid RegisterCustomerRequest request){
        String message = customerService.createCustomer(request);
        return new ResponseEntity<>(new ApiResponse(message, HttpStatus.CREATED.value(), true), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomer(@PathVariable String id) {
        return new ResponseEntity<>(customerService.getCustomer(id), HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestParam(defaultValue="0") int pageNo,
                                    @RequestParam(defaultValue="10") int pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return new ResponseEntity<>(customerService.getAllCustomers(pageable), HttpStatus.OK);
    }

}
