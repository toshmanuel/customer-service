package com.crowninitiative.customerservice.repository;

import com.crowninitiative.customerservice.model.Billing;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BillingRepository extends MongoRepository<Billing, String> {

    Optional<Billing> findBillingByAccountNumber(String accountNumber);
}
