package com.crowninitiative.customerservice.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class Billing {
    @Id
    private String id;
    private String accountNumber;
    private Tariff tariff;

    public Billing(String accountNumber, Tariff tariff) {
        this.accountNumber = accountNumber;
        this.tariff = tariff;
    }
}
