package com.crowninitiative.customerservice.model;

public enum Tariff {
    PREPAID,
    POSTPAID,
}
